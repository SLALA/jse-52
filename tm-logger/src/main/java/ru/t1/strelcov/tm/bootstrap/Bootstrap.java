package ru.t1.strelcov.tm.bootstrap;

import lombok.Getter;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IPropertyService;
import ru.t1.strelcov.tm.api.IReceiverService;
import ru.t1.strelcov.tm.listener.LoggerListener;
import ru.t1.strelcov.tm.service.PropertyService;
import ru.t1.strelcov.tm.service.ReceiverService;

@Getter
public final class Bootstrap {

    public void run(@Nullable final String[] args) {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(propertyService.getMQConnectionFactory());
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LoggerListener());
    }

}
