package ru.t1.strelcov.tm.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

@UtilityClass
public final class CryptUtil {

    @NotNull
    final String TYPE = "AES/ECB/PKCS5Padding";

    @SneakyThrows
    @NotNull
    static SecretKeySpec getKey(@NotNull final String secret) {
        @NotNull final MessageDigest sha = MessageDigest.getInstance("SHA-1");
        final byte[] key = secret.getBytes(StandardCharsets.UTF_8);
        final byte[] digest = sha.digest(key);
        final byte[] secretBytes = Arrays.copyOf(digest, 16);
        return new SecretKeySpec(secretBytes, "AES");
    }

    @SneakyThrows
    @NotNull
    public static String encrypt(@NotNull final String string, @NotNull final String secret) {
        @NotNull final SecretKeySpec key = getKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance(TYPE);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        final byte[] bytes = string.getBytes(StandardCharsets.UTF_8);
        return Base64.getEncoder().encodeToString(cipher.doFinal(bytes));
    }

    @SneakyThrows
    @NotNull
    public static String decrypt(@NotNull final String string, @NotNull final String secret) {
        @NotNull final SecretKeySpec key = getKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance(TYPE);
        cipher.init(Cipher.DECRYPT_MODE, key);
        return new String(cipher.doFinal(Base64.getDecoder().decode(string)));
    }

}
