package ru.t1.strelcov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.model.IProjectRepository;
import ru.t1.strelcov.tm.api.repository.model.IUserRepository;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.model.IProjectService;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.repository.model.ProjectRepository;
import ru.t1.strelcov.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.Optional;


public final class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    public IProjectRepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

    @NotNull
    public IUserRepository getUserRepository(@NotNull final EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    @NotNull
    @Override
    public Project add(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = getUserRepository(entityManager);
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = Optional.ofNullable(userRepository.findById(userId)).orElseThrow(EntityNotFoundException::new);
            final Project project;
            if (Optional.ofNullable(description).filter((i) -> !i.isEmpty()).isPresent())
                project = new Project(user, name, description);
            else
                project = new Project(user, name);
            projectRepository.add(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public Project removeProjectWithTasksById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Project project = Optional.ofNullable(projectRepository.removeById(userId, projectId)).orElseThrow(EntityNotFoundException::new);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
