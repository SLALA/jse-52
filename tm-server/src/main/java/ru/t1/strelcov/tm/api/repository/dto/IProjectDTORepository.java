package ru.t1.strelcov.tm.api.repository.dto;

import ru.t1.strelcov.tm.dto.model.ProjectDTO;

public interface IProjectDTORepository extends IBusinessDTORepository<ProjectDTO> {

}
