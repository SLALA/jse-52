package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.dto.request.UserLoginRequest;
import ru.t1.strelcov.tm.dto.request.UserLogoutRequest;
import ru.t1.strelcov.tm.dto.request.UserProfileRequest;
import ru.t1.strelcov.tm.marker.IntegrationCategory;
import ru.t1.strelcov.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final Integer port = propertyService.getServerPort();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @Nullable
    private String token;

    @Before
    public void before() {
    }

    @After
    public void after() {
    }

    @Test
    public void loginTest() {
        Assert.assertNotNull(authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken());
    }

    @Test
    public void logoutTest() {
        @NotNull final String token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        Assert.assertNotNull(authEndpoint.logout(new UserLogoutRequest(token)));
    }

    @Test
    public void getProfileTest() {
        @NotNull final String token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        Assert.assertNotNull(authEndpoint.getProfile(new UserProfileRequest(token)).getUser());
    }

}
